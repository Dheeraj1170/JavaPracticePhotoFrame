package com.dheeraj.practice.model;

public class PhotoFrame {

	private int id;
	private int length;
	private int breadth;
	private String material;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public PhotoFrame(int id, int length, int breadth, String material) {
		this.id = id;
		this.length = length;
		this.breadth = breadth;
		this.material = material;
	}

	public PhotoFrame() {

	}

}
