package com.dheeraj.practice.model;

import java.util.Scanner;

import com.dheeraj.practice.PhotoFrame;

public class Solution {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		PhotoFrame[] photoFrames = new PhotoFrame[size];
		String inputMaterial = null;
		for (int i = 0; i < size; i++) {
			int id = sc.nextInt();
			int length = sc.nextInt();
			int breadth = sc.nextInt();
			String material = sc.next();
			photoFrames[i] = new PhotoFrame(id, length, breadth, material);
		}

		inputMaterial = sc.next();

		float avg = getAverageAreaOfPhotoFrameByMaterial(photoFrames, inputMaterial);
		PhotoFrame secondFrameObj = findPhotoFrameWithSecondLargestBreadth(photoFrames);

		if (avg > 0) {
			System.out.println(avg);
		} else {
			System.out.println("No such Photo Frame available");
		}

		if (null != secondFrameObj) {
			System.out.println("Id-" + secondFrameObj.getId());
			System.out.println("Length-" + secondFrameObj.getLength());
			System.out.println("Breadth-" + secondFrameObj.getBreadth());
			System.out.println("Material-" + secondFrameObj.getMaterial());
		} else {
			System.out.println("No such Photo Frame available");
		}
	}

	private static float getAverageAreaOfPhotoFrameByMaterial(PhotoFrame[] photoFrames, String material) {

		float areaSum = 0;
		float count = 0;
		float avg = 0;
		for (PhotoFrame frameObj : photoFrames) {
			if (material.equalsIgnoreCase(frameObj.getMaterial())) {
				areaSum = areaSum + (frameObj.getLength() * frameObj.getBreadth());
				count++;
			}
		}

		if (areaSum != 0 && count != 0) {
			avg = areaSum / count;
		}

		if (avg < 25) {
			return 0;
		} else {
			return avg;
		}

	}

	private static PhotoFrame findPhotoFrameWithSecondLargestBreadth(PhotoFrame[] photoFrames) {

		int longestBreadth = 0;
		int secondLongestBreadth = 0;
		PhotoFrame secondFrameObj = null;
		for (PhotoFrame frameObj : photoFrames) {
			if (frameObj.getBreadth() > longestBreadth) {
				longestBreadth = frameObj.getBreadth();
			}
		}

		for (PhotoFrame frameObj : photoFrames) {
			if (frameObj.getBreadth() > secondLongestBreadth && frameObj.getBreadth() < longestBreadth) {
				secondLongestBreadth = frameObj.getBreadth();
				secondFrameObj = frameObj;
			}
		}

		if (secondFrameObj.getLength() < 5 && secondFrameObj.getBreadth() < 5) {
			return null;
		} else {
			return secondFrameObj;
		}
	}
}
